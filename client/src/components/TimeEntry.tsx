import { CiPlay1, CiStop1 } from "react-icons/ci";
import { HiDotsVertical } from "react-icons/hi";
import { useAppDispatch, useAppSelector } from "../hooks";
import moment from "moment";

import {
  incrementDuration,
  updateTimeEntry,
  deleteTimeEntry,
  toggleEditMenu,
} from "../features/timeEntries";

const TimeEntry = () => {
  const dispatch = useAppDispatch();

  // --- Selecting timeEntries from Data Store ---
  const timeEntries = useAppSelector((state) => state.timeEntries.value);

  return (
    <div className=" bg-slate-200 pt-4 px-6 pb-8 rounded-md">
      <h2 className="text-xl font-semibold mb-5">Today</h2>
      <div className="flex flex-col justify-center gap-y-3">
        {timeEntries.map((entry) => (
          <div
            key={entry.id}
            className="relative bg-white py-6 px-8 rounded-md flex items-center justify-between"
          >
            <div className="pr-8">
              <p className="font-semibold">{entry.taskName}</p>
              <p className="text-xs text-gray-500">{entry.description}</p>
            </div>
            <div className="flex items-center gap-4">
              <p className="font-semibold mr-6">
                {moment.utc(entry.duration * 1000).format("H:mm:ss")}
              </p>
              {!entry.timerOn && (
                <button
                  onClick={() => {
                    const interval = setInterval(
                      dispatch,
                      1000,
                      incrementDuration({
                        id: entry.id,
                      })
                    );
                    dispatch(
                      updateTimeEntry({
                        id: entry.id,
                        taskName: entry.taskName,
                        description: entry.description,
                        timerOn: true,
                        interval,
                      })
                    );
                  }}
                >
                  <CiPlay1 size={24} />
                </button>
              )}
              {entry.timerOn && (
                <button
                  onClick={() => {
                    clearInterval(entry.interval);
                    dispatch(
                      updateTimeEntry({
                        id: entry.id,
                        taskName: entry.taskName,
                        description: entry.description,
                        timerOn: false,
                      })
                    );
                  }}
                >
                  <CiStop1 size={24} />
                </button>
              )}

              <button
                onClick={() => {
                  dispatch(
                    toggleEditMenu({
                      id: entry.id,
                      editMenuOn: !entry.editMenuOn,
                    })
                  );
                }}
              >
                <HiDotsVertical size={20} />
              </button>
            </div>
            {entry.editMenuOn && (
              <div className="absolute -right-16 -top-3 border-2 px-3 py-2 bg-white">
                <button
                  onClick={() => {
                    clearInterval(entry.interval);
                    dispatch(
                      updateTimeEntry({
                        id: entry.id,
                        taskName: entry.taskName,
                        description: entry.description,
                        timerOn: false,
                      })
                    );
                    dispatch(deleteTimeEntry({ id: entry.id }));
                  }}
                >
                  Delete Entry
                </button>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default TimeEntry;
