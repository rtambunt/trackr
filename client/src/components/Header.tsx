import Logo from "../assets/logo-profile.png";

const Header = () => {
  return (
    <div>
      <img src={Logo} alt="Trackr Logo" className="mt-10 mb-8 ml-10 w-36" />
      <hr />
    </div>
  );
};

export default Header;
