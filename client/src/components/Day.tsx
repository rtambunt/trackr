import TimeEntry from "./TimeEntry";

const Day = () => {
  return (
    <div className=" bg-slate-200 pt-4 px-6 pb-8 rounded-md">
      <h2 className="text-xl font-semibold mb-5">Today</h2>
      <TimeEntry />
    </div>
  );
};

export default Day;
