import { useState } from "react";
import { CiCirclePlus } from "react-icons/ci";
import { useAppDispatch, useAppSelector } from "../hooks";

import { addTimeEntry } from "../features/timeEntries";

const TimeInput = () => {
  const [taskNameInput, setTaskNameInput] = useState("");
  const timeEntries = useAppSelector((state) => state.timeEntries.value);

  const dispatch = useAppDispatch();

  return (
    <div className=" bg-slate-200 pt-4 px-6 pb-8 rounded-md">
      <div className="bg-white py-6 px-8 rounded-md flex flex-col md:flex-row justify-between">
        <input
          type="text"
          placeholder="I'm working on..."
          onChange={(e) => {
            setTaskNameInput(e.target.value);
          }}
          className="outline-0 w-4/5 mb-6 md:mb-0"
        />
        <div className="flex justify-center items-center">
          <button
            onClick={() => {
              if (taskNameInput.trim().length > 0) {
                dispatch(
                  addTimeEntry({
                    id: timeEntries.length + 1,
                    taskName: taskNameInput,
                    description: "",
                    duration: 0,
                    timerOn: false,
                    interval: undefined,
                    editMenuOn: false,
                  })
                );
              } else {
                alert("Please Enter a Non-Empty Task");
              }
            }}
            className="flex items-center justify-center gap-x-3 text-white bg-blue-400 px-4 py-2 rounded-md"
          >
            <p>Add</p>
            <CiCirclePlus stroke-width={1} size={20} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default TimeInput;
