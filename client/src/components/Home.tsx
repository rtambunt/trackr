import TimeInput from "./TimeInput";
import TimeEntry from "./TimeEntry";
const Home = () => {
  return (
    <div className="mx-auto w-4/5 flex flex-col gap-y-10">
      <TimeInput />
      <TimeEntry />
    </div>
  );
};

export default Home;
