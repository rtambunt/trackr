import { createSlice } from "@reduxjs/toolkit";

export const timeEntriesSlice = createSlice({
  name: "timeEntries",
  initialState: {
    value: [
      {
        id: 1,
        taskName: "Code",
        description: "Work on Trackr project",
        duration: 100,
        timerOn: false,
        interval: undefined,
        editMenuOn: false,
      },
      {
        id: 2,
        taskName: "Run",
        description: "Easy 2 Mile Run",
        duration: 30,
        timerOn: false,
        interval: undefined,
        editMenuOn: false,
      },
    ],
  },
  reducers: {
    addTimeEntry: (state, action) => {
      state.value.push(action.payload);
    },
    updateTimeEntry: (state, action) => {
      state.value.map((entry) => {
        if (entry.id === action.payload.id) {
          entry.taskName = action.payload.taskName;
          entry.description = action.payload.description;
          entry.timerOn = action.payload.timerOn;
          entry.interval = action.payload.interval;
        }
      });
    },
    toggleEditMenu: (state, action) => {
      state.value.map((entry) => {
        if (entry.id === action.payload.id) {
          entry.editMenuOn = action.payload.editMenuOn;
        }
      });
    },
    deleteTimeEntry: (state, action) => {
      
      state.value = state.value.filter((user) => user.id !== action.payload.id);
    },
    incrementDuration: (state, action) => {
      state.value.map((entry) => {
        if (entry.id == action.payload.id) {
          entry.duration += 1;
        }
      });
    },
  },
});

export const {
  addTimeEntry,
  updateTimeEntry,
  deleteTimeEntry,
  incrementDuration,
  toggleEditMenu,
} = timeEntriesSlice.actions;

export default timeEntriesSlice.reducer;
