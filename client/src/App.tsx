// import {
//   ApolloClient,
//   InMemoryCache,
//   ApolloProvider,
//   gql,
// } from "@apollo/client";
import { Provider } from "react-redux";

import { store } from "./store";
import Header from "./components/Header";
import Home from "./components/Home";

function App() {
  return (
    <div className="text-gray-800">
      <Header />
      <Provider store={store}>
        <Home />
      </Provider>
    </div>
  );
}

export default App;
