const { ApolloServer } = require("apollo-server");
const mongoose = require("mongoose");

require("dotenv").config({ path: ".env.local" });

const MONGODB = `mongodb+srv://rtambunting:${process.env.MONGODB_PW}@cluster0.wxflh1g.mongodb.net/?retryWrites=true&w=majority`;

// --- Apollo Server ---

const typeDefs = require("./graphql/typeDefs");
const resolvers = require("./graphql/resolvers");

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

mongoose
  .connect(MONGODB, { useNewUrlParser: true })
  .then(() => {
    console.log("Mongoose Connection Successful!");
    return server.listen({ port: 4000 });
  })
  .then((res) => {
    console.log(`Server running at ${res.url}`);
  });
