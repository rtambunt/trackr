const { model, Schema } = require("mongoose");

const timeEntrySchema = new Schema({
  taskName: String,
  category: String,
  createdAt: String,
});

module.exports = model("Time Entry", timeEntrySchema);
