const TimeEntry = require("../models/TimeEntry");

module.exports = {
  Query: {
    timeEntry: async (_, { ID }) => {
      return await TimeEntry.findById(ID);
    },
    timeEntries: async () => await TimeEntry.find().sort({ createdAt: -1 }),
  },
  Mutation: {
    createTimeEntry: async (_, { timeEntryInput: { taskName, category } }) => {
      const createdTimeEntry = new TimeEntry({
        taskName: taskName,
        category: category,
        createdAt: new Date().toISOString(),
      });

      const res = await createdTimeEntry.save();

      console.log(res._doc);
      return {
        id: res.id,
        ...res._doc,
      };
    },
    deleteTimeEntry: async (_, { ID }) => {
      const wasDeleted = (await TimeEntry.deleteOne({ _id: ID })).deletedCount;

      return wasDeleted;
    },
    editTimeEntry: async (
      _,
      { ID, timeEntryInput: { taskName, category } }
    ) => {
      const wasEdited = (
        await TimeEntry.updateOne(
          { _id: ID },
          { taskName: taskName, category: category }
        )
      ).modifiedCount;

      return wasEdited;
    },
  },
};
