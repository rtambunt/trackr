const { gql } = require("apollo-server");

module.exports = gql`
  type TimeEntry {
    taskName: String
    category: String
    createdAt: String
  }

  input TimeEntryInput {
    taskName: String
    category: String
  }

  type Query {
    timeEntry(ID: ID!): TimeEntry!
    timeEntries: [TimeEntry]
  }

  type Mutation {
    createTimeEntry(timeEntryInput: TimeEntryInput): TimeEntry!
    deleteTimeEntry(ID: ID!): Boolean
    editTimeEntry(ID: ID!, timeEntryInput: TimeEntryInput): Boolean
  }
`;
